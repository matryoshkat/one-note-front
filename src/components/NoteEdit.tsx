import * as React from 'react';
import * as actions from '../Actions';
import NoteModel from '../models/NoteModel';
import { RootState } from '../AppState';
import Title from './custom-components/Title'
import NoteListModel from '../models/NoteListModel';
import { connect } from 'react-redux';
import { Divider, Paper, IconButton, Icon } from '@material-ui/core';
import { Dispatch } from 'redux';

interface INoteEditProps{
    groups: ReadonlyArray<NoteListModel>;
    activeNote: NoteModel;
    stateGroupId: number;
    stateNoteId: number;
    urlGroupId: number;
    urlNoteId: number;
    togglePanels?: () => void;
    updateNote: (note: NoteModel) => void;
}

interface INoteEditState {
    noteContent: string;
    noteTitle: string;
}

class NoteEdit extends React.Component<INoteEditProps, INoteEditState> {
    private timeOuthandler: number;
    constructor(props: INoteEditProps){
        super(props);
        if(props.activeNote != null){
            this.state = {
                noteContent: props.activeNote.content || "",
                noteTitle: ""
            }
        } else {
            this.state = {
                noteContent: "",
                noteTitle: ""
            }
        }

    }

    private titleChanged(title:string){
        if(title == "") return;
        this.state = {
            noteContent: this.state.noteContent,
            noteTitle: title,
        }
        clearTimeout(this.timeOuthandler)
        // this.timeOuthandler = 1;
        this.timeOuthandler = window.setTimeout(()=>{
            const note: NoteModel = new NoteModel();
            note.title = title;
            note.content = this.state.noteContent;
            note.listId = this.props.urlGroupId;
            note.id = this.props.urlNoteId;
            this.props.updateNote(note);
            console.log("Note is", note);
        }, 1000);
    }

    private contentChanged(content: string){
        this.props.activeNote.content = content;
        this.setState({
            noteContent: content,
        });
        clearTimeout(this.timeOuthandler);
        this.timeOuthandler = window.setTimeout(() => {
            const note = this.createNote(
                this.state.noteTitle,
                this.state.noteContent,
                this.props.activeNote.id,
                this.props.activeNote.listId
            );

            this.props.updateNote(note);
            console.log("Note is ", note);
        }, 1000)
    }

    private createNote(title: string, content:string, id:number, listId:number):NoteModel{
        const note: NoteModel = new NoteModel();
        note.title = title;
        note.content = this.state.noteContent;
        note.listId = this.props.urlGroupId;
        note.id = this.props.urlNoteId;
        return note;
    }

    static getDerivedStateFromProps(props: INoteEditProps, state: INoteEditState) {
        if(props.activeNote == null) return null;
        return {
            ...state,
            noteContent: props.activeNote.content,
            noteTitle: props.activeNote.title
        };
    }

    public render() {
        // console.log("State note id", this.props.stateNoteId);
        // console.log("Url note id", this.props.urlNoteId);
        
        if (this.props.activeNote == null) return (<h1>Note Id undefined</h1>);
        let note = this.props.activeNote;

        return(
            <div className="note-details-container">
                <div className="menu-btn">
                    <IconButton
                        onClick={() => this.props.togglePanels()}
                    >
                        <Icon>menu</Icon>
                    </IconButton>
                </div>
                <div className="title-container">
                    <div className="title">
                        <Title 
                            text={note.title} 
                            onChangeHandler={(e) => this.titleChanged(e)}
                        />
                    </div>
                </div>
                <Divider className="divider"/>
                <div className="content">
                    <Paper className="paper" elevation={4}>
                        <textarea
                            autoFocus
                            placeholder="Enter note text"
                            value={this.state.noteContent || ""}
                            onChange={(e) => {
                                this.contentChanged(e.target.value as string);
                                // this.props.activeNote.content = e.target.value;
                                // this.setState({
                                //     noteContent: e.target.value as string,
                                // });
                            }}
                        />
                    </Paper>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    updateNote: (note: NoteModel) => dispatch(actions.updateNote(note)),
})

const mapStateToProps = (state: RootState) => ({
    groups: state.notes,
    stateGroupId: state.activeGroupId,
    stateNoteId: state.activeNoteId,
    activeNote: state.activeNote,
})

export default connect(mapStateToProps, mapDispatchToProps)(NoteEdit);

