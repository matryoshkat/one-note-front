import React, { ChangeEvent } from "react";
import AddButton from "./AddButton";
import { TextField, CircularProgress } from "@material-ui/core";

interface IProps {
    placeholder: string;
    addButtonCallback: (text: string, itemAddedCallback: ()=> void) => void;
    isLoading: boolean;
}

interface IState {
    isLabelHidden: boolean;
    fieldText: string;
    isFieldTextDisabled: boolean;
    // isLoading: boolean;
}

export default class AdditemContainer extends React.Component<IProps, IState>{
    constructor(props: IProps){
        super(props);
        this.state = {
            isLabelHidden: false,
            fieldText: "",
            isFieldTextDisabled: false,
            // isLoading: false,
        }
    }

    private handleChanges(e: any):void {
        let inputText: string = e.target.value as string;
        if( inputText.length >  23) return;
        this.setState({
            fieldText: e.target.value,
        })
    }

    // private toggleLoading(){
    //     this.setState({
    //         isLoading: !this.state.isLoading,
    //     })
    // }


    //Called when item was successfully(or not) retained;
    private itemAdded(){
        this.setState({
            isFieldTextDisabled: false,
            fieldText: ""
        });
    }

    private onBtnClick(){
        //TODO: Whitespace check
        if(this.state.fieldText == "") return;
        // this.toggleLoading();
        this.setState({
            isFieldTextDisabled: true,
        });
        this.props.addButtonCallback(this.state.fieldText, () => this.itemAdded());
    }

    public render() {
        const progress: JSX.Element = (
        <div className="progress">
            <CircularProgress
                size={25} />
        </div>)

        return(
            <div className="add-item-container">

                {this.props.isLoading ? progress : null }

                <TextField 
                    className="add-item-text"
                    InputLabelProps={{
                        disabled:true,
                    }}
                    InputProps={{
                        placeholder: this.props.placeholder,
                        disabled: this.state.isFieldTextDisabled
                    }}
                    
                    //label={this.state.isLabelHidden ? "" : this.props.placeholder}
                    // onClick={() => {
                    //     if(this.state.isLabelHidden) return;
                    //     this.setState({
                    //         isLabelHidden: !this.state.isLabelHidden,
                    //     })
                    // }}
                    // onBlur={() => {
                    //     if(this.state.fieldText != "") return;
                    //     if(!this.state.isLabelHidden) return;
                    //     this.setState({
                    //         isLabelHidden: !this.state.isLabelHidden,
                    //     })
                    // }}
                    value={this.state.fieldText}
                    onChange={(e) => this.handleChanges(e)}
                />
                <AddButton 
                    onClick={() => this.onBtnClick()} 
                    isEnabled={!this.props.isLoading}/>
            </div>
        )
    }
}