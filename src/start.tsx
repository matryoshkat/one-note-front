import * as React from 'react';
import { createStore, Store, applyMiddleware } from 'redux';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import '../styles/main.less';
import { rootReducer } from './RootReducer';
import Thunk from 'redux-thunk'

export const store: Store = createStore(rootReducer,
    applyMiddleware(Thunk));

render((
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>


),
    document.getElementById('root'),
);
