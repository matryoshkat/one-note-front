import * as React from 'react';
import { Route } from 'react-router-dom';
import { RootState } from '../AppState';
import NoteListModel from '../models/NoteListModel';
import { Dispatch } from 'redux';
import * as actions from '../Actions';
import { connect } from 'react-redux';
import { List, ListItem, ListItemText } from '@material-ui/core';
import AdditemContainer from './custom-components/AddItem';
import NoteModel from '../models/NoteModel';

interface INotesProps {
    groups: Array<NoteListModel>;
    activeGroupId: number;
    activeGroup: NoteListModel;
    activeNoteId: number;
    onNoteClick?: (id:number) => void;
    retainNote: (note: NoteModel, noteAddedCallback:()=>void) => any;
    setActiveNoteId: (id: number) => any;
    setActiveNoteState: (note: NoteModel) => any;
    isNoteLoading: boolean;
    urlGroupId?: number;
    urlPath: string;
}

class Notes extends React.Component<INotesProps>{

    constructor(props: INotesProps) {
        super(props);
        // console.log("Notes logs");
        // console.log("Notes props:", this.props);

        if(this.props.urlPath.length > 3) {
            let urlParams: Array<string>;
             urlParams = this.props.urlPath.split("/");
             if(urlParams.length > 2) {
                let noteId: number = urlParams[2] as any as number;
                this.setActiveNote(noteId);
            }
        }

    }

    private setActiveNote(noteId: number){
        if(this.props.activeGroup == null) return;
        this.props.setActiveNoteId(noteId);
        let activeNote = this.props.activeGroup.notes.find(note => note.id == noteId);
        if (activeNote == undefined) return;
        this.props.setActiveNoteState(activeNote);

    }

    private createNewNote(noteName: string, noteAddedCallback:()=> void) {
         const listId: number = this.props.urlGroupId == 0 ? this.props.activeGroupId : this.props.urlGroupId;
         const note: NoteModel = new NoteModel();
         note.title = noteName;
         note.listId = listId;
         this.props.retainNote(note, noteAddedCallback);
    }

    public render() {
        let notes: JSX.Element[] = [];
        if(this.props.activeGroup != null) {
            notes = (this.props.activeGroup.notes.map(
                (note) => {
                    return <Route key={note.id} render={
                        ({ history, match }) => (
                            <ListItem
                                className="list-item"
                                button
                                selected={this.props.activeNoteId == note.id}
                                onClick={() => {
                                    this.setActiveNote(note.id);
                                    this.props.onNoteClick(note.id);
                                    history.push(match.url + "/" + note.id.toString());
                                }}
                            >
                                <ListItemText primary={note.title}/>
                            </ListItem>
                        )
                    } />
                }
            ));
        }  
        return (  

            <div className="notes">
                <List>
                    {notes}
                </List>

                <AdditemContainer
                    isLoading={this.props.isNoteLoading}
                    placeholder="Note Title" 
                    addButtonCallback={(noteName:string, noteAddedCallback: ()=>void) => this.createNewNote(noteName, noteAddedCallback)}
                />

            </div>            
        )}
}



const mapStateToProps = (state: RootState) => ({
    groups: state.notes,
    activeGroupId: state.activeGroupId,
    activeNoteId: state.activeNoteId,
    isNoteLoading: state.isNoteAdding,
    activeGroup: state.activeGroup,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    onNoteClick: (id:number) => dispatch(actions.changeActiveNote(id)),
    retainNote: (note: NoteModel, noteAddedCallback:()=> void) => dispatch(actions.sendNewNote(note, noteAddedCallback)),
    setActiveNoteId: (id: number) => dispatch(actions.changeActiveNote(id)),
    setActiveNoteState: (note: NoteModel) => dispatch(actions.setActiveNote(note)),

})

export default connect(mapStateToProps, mapDispatchToProps)(Notes);