import { TextField, Input } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React from "react";

interface IProps {
    text:string;
    onChangeHandler: (e: string) => void;
}

interface IState {
    isEdit: boolean;
    text: string;
}

export default class Title extends React.Component<IProps, IState> {
    constructor(props: IProps){ 
        super(props);
        this.state = {
            isEdit: false,
            text: "",
        }
    }

    private handleChanges(e:any) {
        this.setState({
            text: e.target.value as string,
        })
        this.props.onChangeHandler(e.target.value as string);
    }

    public render(){
        this.state  = {
            isEdit: this.state.isEdit,
            text: this.props.text
        };
        // console.log("Title render. Props:", this.props);
        // console.log("Title render. State:", this.state);
        if(this.state.isEdit)
            return (
                <TextField
                    className="edit"
                    autoFocus
                    placeholder="Note name"
                    defaultValue={this.state.text}
                    onBlur={() => {
                        if(this.state.text == "") return;
                        this.setState({
                            isEdit: !this.state.isEdit,
                        })
                    }}
                    onChange={(e) => this.handleChanges(e) }
                />
            )
        else 
            return (
                <Typography
                    className="text"
                    variant="display2"
                    onClick={() => {
                        this.setState({
                            isEdit: !this.state.isEdit,
                        })
                    }}
                >
                    {this.state.text}
                </Typography>
            )
    }
}

// const styles = {
//     resize:{
//         fontSize:50
//     },
// }

const styles = {
    textField: {
    fontSize: 50, //works!
 }
}