
export default class NoteModel {
    id: number;
    listId: number;
    title: string;
    content: string;
}