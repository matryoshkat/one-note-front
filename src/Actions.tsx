import { action } from 'typesafe-actions'
import NoteListModel from './models/NoteListModel';
import { Dispatch } from 'redux';
import { Api } from './Api';
import NoteModel from './models/NoteModel';

export const GROUP_CHANGED = "GROUP_CHANGED";
export const NOTE_CHANGED = "NOTE_CHANGED";
export const NOTE_ADDED = "NOTE_ADDED";
export const GROUP_ADDED = "GROUP_ADDED";
export const NOTES_COLLECTION_CHANGED = "NOTES_COLLECTION_CHANGED";
export const NOTE_RESET = "reset-note";
// export const GROUPS_LOADING = "groups-loading";
 export const GROUP_ITEM_RETAINING = "group-item-retaining";
 export const GROUP_ITEM_RETAINED = "group-item-retained";
 export const ITEMS_FETCHING = "items-fetching";
 export const ITEMS_FETCHED = "items-fetched";
 export const NOTE_RETAINING = "note-retaining";
 export const NOTE_RETAINED = "note-retained";
 export const NOTE_UPDATED = "note-updated";
 export const SET_ACTIVE_NOTE = "SET_ACTIVE_NOTE";
 export const SET_ACTIVE_GROUP = "SET_ACTIVE_GROUP";
 export const NOTE_UPDATING = "NOTE_UPDATING";

export const changeActiveGroup = (groupId: number)=> action(GROUP_CHANGED, groupId);
export const changeActiveNote = (noteId: number) =>  action(NOTE_CHANGED, noteId);
export const resetActiveNote = () => action(NOTE_RESET);
export const retainGroupItem = () => action(GROUP_ITEM_RETAINING);
export const groupItemRetained = () => action(GROUP_ITEM_RETAINED);
export const noteRetaining = () => action(NOTE_RETAINING);
export const noteRetained = () => action(NOTE_RETAINED);
export const noteUpdating = () => action(NOTE_UPDATING);
export const noteUpdated = (note: NoteModel) => action(NOTE_UPDATED, note);
export const setActiveNote = (note: NoteModel) => action(SET_ACTIVE_NOTE, note);
export const setActiveGroup = (group: NoteListModel) => action(SET_ACTIVE_GROUP, group);
// export const groupsLoading = () => action(GROUPS_LOADING);
export const changeNotes = (notes: ReadonlyArray<NoteListModel>) => action(NOTES_COLLECTION_CHANGED, notes);
export const addNote = (note: NoteModel) => action(NOTE_ADDED, note);
export const itemsFetching = () => action(ITEMS_FETCHING);
export const itemsFetched = () => action(ITEMS_FETCHED);
export const addGroup = (group: NoteListModel) => action(GROUP_ADDED, group);


export const fetchNotes = ():any => 
    (dispatch: Dispatch) => {
        // dispatch(itemsFetching());
        Api.fetchNotes()
        .then(r => r.json(), e => console.error("Error when fetching"))
        .then(json => {
            // console.log("Notes json is ", json)
            json.map((group:any) => {
                const newGroup: NoteListModel = new NoteListModel();
                newGroup.name = group.name;
                newGroup.id = group.id as number;
                newGroup.notes = group.notes;
                dispatch(addGroup(newGroup));
            });
            dispatch(itemsFetched());
        });
    }


export const sendGroupItem = (groupName: string, groupAddedCallback: () => void):any => (
    dispatch: Dispatch, getState: () => any) => {
        // console.log("Gotcha ", groupName);

        dispatch(retainGroupItem());
        // setTimeout( () => 
        //     dispatch(groupItemRetained()), 
        //     500);
            
        Api.addGroup(groupName)
        .then(r => r.json(), e => console.error("Error when trying to send new group", e))
        .then(json => {
            const group: NoteListModel = new NoteListModel();
            group.name = groupName;
            group.id = json as number;
            group.notes = [];
            dispatch(addGroup(group));
            dispatch(groupItemRetained());
            groupAddedCallback();
        }); 
        //group added with id
}

export const sendNewNote = (note: NoteModel, noteAddedCallBack: ()=> void):any => (dispatch: Dispatch) => {
    dispatch(noteRetaining());
    Api.addNote(note)
    .then(r => r.json(), e => console.log("Error when trying retain note:", e))
    .then(json => {
        note.id = json as number;
        dispatch(addNote(note));
        dispatch(noteRetained());
        noteAddedCallBack();
    })
}

export const updateNote = (note: NoteModel):any => (dispatch: Dispatch) => {
    dispatch(noteUpdating());
    Api.updateNote(note)
    .then(r => r.json(), e => console.error("Error when updating note:",e))
    .then(j => {
        dispatch(noteUpdated(note));
    })
}