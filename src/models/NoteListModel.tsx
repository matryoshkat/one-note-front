import NoteModel from "./NoteModel";

export default class NoteListModel{
    id: number;
    name: string;
    notes: Array<NoteModel>;
}