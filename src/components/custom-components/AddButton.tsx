import React from "react";
import { Button } from "@material-ui/core";
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/lightGreen';

interface IProps {
    onClick: () => void;
    isEnabled: boolean;
}

export default class AddButton extends React.Component<IProps> {
    public render() {
        return (
            <MuiThemeProvider theme={theme} >
                <Button
                    className="add-btn"
                    disabled={!this.props.isEnabled}
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        this.props.onClick();
                    }}
                >
                    Add
            </Button>
            </MuiThemeProvider>
        )
    }
}

// withStyles({})(AddButton)

const theme = createMuiTheme({
    palette: {
        primary: green, 
    },
})