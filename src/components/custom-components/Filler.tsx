import React from "react";
import { Typography } from "@material-ui/core";

export default class Filler extends React.Component<{}> {
    render(){
        return(
            <div className="filler">
                <Typography
                    variant="display3" 
                >Select Note</Typography>
            </div>
        )
    }
}