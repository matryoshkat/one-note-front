import NoteListModel from "./models/NoteListModel";
import { ActionType, StateType } from "typesafe-actions";
import { rootReducer } from "./RootReducer";
import * as actions from './Actions'
import NoteModel from "./models/NoteModel";


export type State = {
    readonly notes: ReadonlyArray<NoteListModel>
    readonly activeGroupId: number;
    readonly activeNoteId: number;
    readonly isGroupItemAdding: boolean;
    readonly isItemsFetching: boolean;
    readonly isNoteAdding: boolean;
    readonly activeNote: NoteModel;
    readonly activeGroup: NoteListModel;
    readonly isNoteUpdating: boolean;
}

export type NotesAction = ActionType<typeof actions>;
export type RootState = StateType<typeof rootReducer>;