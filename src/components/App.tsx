import * as React from 'react';
import NotesGroup from './NotesGroup';
import { Route } from "react-router-dom";
import NoteListModel from '../models/NoteListModel';
import { connect } from 'react-redux';
import { RootState } from '../AppState';
import NoteModel from '../models/NoteModel';
import * as actions from '../Actions';
import { Dispatch, Store } from 'redux';
import Notes from './Notes';
import NoteEdit from './NoteEdit';
import { store } from '../start';
import Filler from './custom-components/Filler';


interface IProps {
        changeNotes: (notes: ReadonlyArray<NoteListModel>) => any;
        fetchNotes: () => any;
        selectedGroupId: number;
        activeNoteId: number;
        notes: ReadonlyArray<NoteListModel>;
        isItemsFetching: boolean;
 }

 interface IState {
     isPanelsHidden: boolean;
 }
class App extends React.Component<IProps,IState> {

    constructor(props: IProps) {
        super(props);
        // console.log("App props:", this.props);
        //this.CreatemockArray();
        this.props.fetchNotes();
        store.subscribe(() => this.listener());
        this.state = {
            isPanelsHidden: false,
        }
        //Fetch data
    }

    private togglePanels(){
        this.setState({
            isPanelsHidden: !this.state.isPanelsHidden,
        })
    }

    private CreatemockArray(): void {
        let noteModel1 = new NoteModel();
        noteModel1.id = 1;
        noteModel1.title = "1st note in first group";
        noteModel1.content = "Note content";
        let noteModel2 = new NoteModel();
        noteModel2.id = 2;
        noteModel2.title = "1st note in sec group";
        noteModel2.content = "Note content";
        let notesList: Array<NoteModel> = [noteModel1];
        let notesList2: Array<NoteModel> = [noteModel2];
        let newListGroup = new NoteListModel();
        let newListGroup2 = new NoteListModel();

        newListGroup.id = 1;
        newListGroup.name = "First List!";
        newListGroup.notes = notesList;

        newListGroup2.id = 2;
        newListGroup2.name = "Second List!";
        newListGroup2.notes = notesList2;

        let notesGroups: ReadonlyArray<NoteListModel> = [newListGroup, newListGroup2];
        this.props.changeNotes(notesGroups);
        // console.log("Props after mock:", this.props);
    }

    private listener():void{
        let nextState: RootState  = store.getState();

        if(nextState.activeNoteId == 0)
            if(this.props.activeNoteId != 0)
                this.setState(this.state);

        // console.log("Next state is", nextState);
        // console.log("Current", this.props);
    }

    public render() {
        // console.log("App render");
        if(this.props.isItemsFetching) return <h1>Loading...</h1>
        const noteDetails: JSX.Element = (
            <Route path="/:groupId/:noteId" render={(props) => {
                // console.log("Router props", props);
                return <NoteEdit 
                            urlGroupId={props.match.params.groupId} 
                            urlNoteId={props.match.params.noteId}
                            togglePanels={() => this.togglePanels()}
                        />;
            }} />
        );

        if(this.state.isPanelsHidden)
            return (
                <div className="container">
                    {noteDetails}
                </div>
            )
        else
            return (
                <div className="container">
                    <Route path="/" render={(props) => {
                        return <NotesGroup routerProps={props} />;
                    }} />

                    <Route path="/" render={() => {
                        return <div className="vertical-line" />
                    }} />

                    <Route exact path="/" render={(props) => {
                        return <Filler />;
                    }} />



                    <Route path="/:id" render={(props) => {
                        return <Notes urlGroupId={props.match.params.id} urlPath={props.location.pathname} />;

                    }} />

                    <Route path="/:id/" render={() => {
                        return <div className="vertical-line" />
                    }} />

                    <Route exact path="/:id" render={(props) => {
                        return <Filler />;
                    }} />

                    {noteDetails}
                </div>
            )  
    }
}

// What appears in props
const mapStateToAppProps = (state: RootState) => ({
    selectedGroupId: state.activeGroupId,
    activeNoteId: state.activeNoteId,
    notes: state.notes,
    isItemsFetching: state.isItemsFetching
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    changeNotes: (notes: ReadonlyArray<NoteListModel>) => dispatch(actions.changeNotes(notes)),
    fetchNotes: () => dispatch(actions.fetchNotes()), 
});


export default connect(mapStateToAppProps, mapDispatchToProps)(App);