import { Reducer, combineReducers } from "redux";
import { State, NotesAction } from "./AppState";
import { ActionType, action } from "typesafe-actions";
import * as actions from './Actions'
import NoteListModel from "./models/NoteListModel";
import NoteModel from "./models/NoteModel";


export const rootReducer: Reducer = combineReducers<State, NotesAction>({
    notes: (state:ReadonlyArray<NoteListModel> = [], action: any) => {
        // console.log("notes reducer entered. Action:", action);          
        // console.log("action.type is", action.type);          
        // console.log("action.NOTES_CHANGED is", actions.NOTES_COLLECTION_CHANGED);
        // console.log(action.type, " === ", actions.NOTES_COLLECTION_CHANGED, " ", action.type === actions.NOTES_COLLECTION_CHANGED);          
        switch (action.type) {
            case actions.NOTE_ADDED:
                const note: NoteModel = action.payload as NoteModel;
                let neededList = state.find(g => g.id == note.listId);
                if(neededList === undefined){
                    console.error("List id didnt found to insert new item");
                    return state;
                }
                neededList.notes.push(note);
                return state.map(list => {
                    if(list.id != note.listId) return list;
                    return neededList;});
            case actions.NOTE_UPDATED:
            {
                //looks shitty .=.
                const updatedNote = action.payload as NoteModel;
                const neededList = state.find(g => g.id == updatedNote.listId);
                let oldNote = neededList.notes.find(n => n.id == updatedNote.id);
                oldNote.content = updatedNote.content;
                oldNote.title = updatedNote.title;
                return [...state];
            }
                    
            case actions.GROUP_ADDED:
                const group: NoteListModel = action.payload as NoteListModel;
                return [...state, group];
            case actions.NOTES_COLLECTION_CHANGED:
                // console.log("NOTES_CHANGED", action);
                return [...action.payload];
            default:
                // console.log("notes reducer: ", state);
                return state;
        }
    },
    activeGroupId: (state = -1, action: ActionType<typeof actions.changeActiveGroup>) => {
        // console.log("entered activeGroupId reducer. Action is", action);
        switch (action.type) {
            case actions.GROUP_CHANGED:
                // console.log("activeGroupId reducer. Action:", action);
                return action.payload;
            default:
                return state;
        }
    },
    activeNoteId: (state = -1, action: any) => {
        switch (action.type){
            case actions.NOTE_CHANGED:
                return action.payload;
            case actions.NOTE_RESET:
                return -1;
            default:
                return state;
        }   
    },
    isGroupItemAdding: (state = false, action: any) => {
        switch (action.type) {
            case actions.GROUP_ITEM_RETAINING:
                return true;
            case actions.GROUP_ITEM_RETAINED:
                return false;
            default:
                return state;
        }
    },
    isItemsFetching: (state = true, action) =>  {
        switch(action.type){
            case actions.ITEMS_FETCHING:
            return true;
            case actions.ITEMS_FETCHED:
            return false;
            default:
            return state;
        }
    },
    isNoteAdding: (state = false, action) => {
        switch(action.type){
            case actions.NOTE_RETAINING:
            return true;
            case actions.NOTE_RETAINED:
            return false;
            default:
            return state;
        }
    },
    activeNote: (state = null, action:any) => {
        switch(action.type) {
            case actions.SET_ACTIVE_NOTE:
            return action.payload;
            default:
            return state;
        }
    },
    activeGroup: (state = null, action:any) => {
        switch(action.type) {
            case actions.SET_ACTIVE_GROUP:
            return action.payload;
            default:
            return state;
        }
    },
    isNoteUpdating: (state = false, action:any) => {
        switch(action.type) {
            case actions.NOTE_UPDATING:
            return true;
            case actions.NOTE_UPDATED:
            return false;
            default:
            return state;
        }
    }
})
