import * as React from 'react';
import NoteListModel from '../models/NoteListModel';
import { Route, RouteComponentProps } from 'react-router-dom';
import { RootState } from '../AppState';
import { Dispatch } from 'redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import * as actions from '../Actions';
import { connect } from 'react-redux';
import { ListItemText } from '@material-ui/core';
import AdditemContainer from './custom-components/AddItem';
import { StaticContext } from 'react-router';


interface IProps {
    groups: Array<NoteListModel>
    setActiveGroupId: (id: number) => any;
    setActiveGroupState: (group: NoteListModel) => any;
    resetActiveNote: () => any;
    retainGroup: (groupName: string, groupAddedCallback: () => void) => any;
    isGroupRetaining: boolean;
    //May be changed to string. And take only routerProps.location.pathname 
    routerProps: RouteComponentProps<any, StaticContext, any>;
    isItemsFetching: boolean;
    activeGroupId: number;
}

class NotesGroup extends React.Component<IProps>{
    constructor(props: IProps) {
        super(props);
        if (this.props.routerProps.location.pathname.length > 1) {
            let urlParams: Array<string>;
            urlParams = this.props.routerProps.location.pathname.split("/");
            if (urlParams.length > 1) {
                let groupId: number = urlParams[1] as any as number;
                this.setActiveGroup(groupId);
            }
        }
    }

    private handleAddButton(groupName: string, groupAddedCallback: () => void) {
        // console.log("Add group pressed. New group name is ", groupName);
        this.props.retainGroup(groupName, groupAddedCallback);
    }

    private setActiveGroup(groupId: number) {
        this.props.setActiveGroupId(groupId);
        let neededGroup = this.props.groups.filter(group => group.id == groupId)[0];
        if (neededGroup == undefined) return;
        this.props.setActiveGroupState(neededGroup);
    }

    public render() {
        // console.log("NoteGroup render");
        return (
            <div className="groups">

                <List>
                {this.props.groups.map((noteList) => {
                    return <Route key={noteList.id} render={({ history, match }) => (
                        
                        <ListItem
                            className="list-item"
                            button
                            selected={this.props.activeGroupId == noteList.id}
                            onClick={() => {
                                this.setActiveGroup(noteList.id);
                                history.push(match.url + noteList.id);
                                this.props.resetActiveNote();
                            }}
                        >
                            <ListItemText primary={noteList.name} />
                        </ListItem>
                    )} />
                })}
                </List>
                
                <AdditemContainer 
                    placeholder="Notebook name" 
                    addButtonCallback={(text: string, groupAddedCallback: () => void) => this.handleAddButton(text, groupAddedCallback)}
                    isLoading={this.props.isGroupRetaining}
                />

            </div>
        )
    }
}

const mapStateToProps = (state: RootState) => ({
    isGroupRetaining: state.isGroupItemAdding,
    groups: state.notes,
    isItemsFetching: state.isItemsFetching,
    activeGroupId: state.activeGroupId,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    setActiveGroupId: (id:number) => dispatch(actions.changeActiveGroup(id)),
    resetActiveNote: () => dispatch(actions.resetActiveNote()),
    retainGroup: (name: string, groupAddedCallback: () => void) => dispatch(actions.sendGroupItem(name, groupAddedCallback)),
    setActiveGroupState: (group: NoteListModel) => dispatch(actions.setActiveGroup(group)),
})

export default connect(mapStateToProps, mapDispatchToProps)(NotesGroup);