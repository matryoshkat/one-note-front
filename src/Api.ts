import { addNote } from "./Actions";
import NoteModel from "./models/NoteModel";

export module Api {
     var _baseUrl: string = "http://176.37.72.62:1638/api/";
     var _headers = new Headers({
        "Content-Type": "application/json"
    });

    export function fetchFromUrl(url: string) {
        const resUrl:string = _baseUrl + url;
        console.log("Getting ", resUrl);
        return fetch(resUrl);
    }

    export function addGroup(groupName: string){
        return fetch(_baseUrl + "notes/addgroup", {
            headers: _headers,
            method: "post",
            body: JSON.stringify(groupName)
        })
    }

    export function addNote(note: NoteModel) {
        return fetch(_baseUrl + "notes/addnote", {
            headers: _headers,
            method: "post",
            body: JSON.stringify(note)
        })
    }

    export function fetchNotes() {
        return fetch(_baseUrl + "notes/");
    }
    
    export function updateNote(note: NoteModel) {
        return fetch(_baseUrl + "notes/updatenote", {
            headers: _headers,
            method: "post",
            body: JSON.stringify(note)
        })
    }
    function getValues(): Promise<any> {
        const url = "values"
        return this.fetchFromUrl(url);
    }
}